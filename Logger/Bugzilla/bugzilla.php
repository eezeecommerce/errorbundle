<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ErrorBundle\Logger\Bugzilla;

use eezeecommerce\ErrorBundle\Logger\AbstractLogger;

/**
 * Description of bugzilla
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class bugzilla extends AbstractLogger
{
    protected function doCall()
    {
        return $this->caller->call($this->json());
    }
}
