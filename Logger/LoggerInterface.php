<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ErrorBundle\Logger;

/**
 * Description of LoggerInterface
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
interface LoggerInterface
{
    /**
     * @return string
     */
    public function getUri();
}
