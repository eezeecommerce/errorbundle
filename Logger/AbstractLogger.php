<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ErrorBundle\Logger;

use Lsw\ApiCallerBundle\Call\HttpPostJson;
use Symfony\Component\DependencyInjection\Container;

/**
 * Description of AbstractLogger
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
abstract class AbstractLogger implements LoggerInterface
{
    /**
     * @var ApiCallerInterface $caller
     */
    protected $caller;
    
    /**
     * @var array $config
     */
    protected $config;
    
    /**
     * @var string $uri
     */
    protected $uri;
    
    /**
     * @var string $key 
     */
    protected $key;

    /**
     * @var array $params
     */
    protected $params;
    
    /**
     * @param ApiCallerInterface $caller
     * @param HttpGetJson $json
     */
    public function __construct(Container $container, array $config)
    {
        $this->caller = $container->get("api_caller");
        $this->config = $config;

        if (is_array($this->config["logging"])) {

            $config = $this->config["logging"];

            if (key_exists("api_key", $config)) {
                $this->setKey($config["api_key"]);
            }
            if (key_exists("uri", $config)) {
                $this->setUri($config["uri"]);
            }
        }
    }
    
    public function setKey($key)
    {
        $this->key = $key;
    }
    
    protected function getKey()
    {
        return $this->key;
    }

    
    public function setUri($uri)
    {
        $this->uri = $uri;
    }
    
    public function getUri()
    {
        return $this->uri;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }
    
    public function call() {

        
        if (null === $this->uri) {
            throw new \InvalidArgumentException("Uri needs to be set");
        }

        $this->params["api_key"] = $this->key;
        $this->params["product"] = "eezeecommerce";
        
        return $this->doCall();
    }

    protected function json()
    {
        return new HttpPostJson($this->uri, $this->params);
    }
    
    abstract protected function doCall();

}
