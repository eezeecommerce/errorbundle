<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/02/16
 * Time: 09:57
 */

namespace eezeecommerce\ErrorBundle\EventSubscriber;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    /**
     * @var Container $container
     */
    private $container;

    /**
     * ExceptionListener constructor.
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();


        if ($exception instanceof HttpExceptionInterface) {
            $code = $exception->getStatusCode();
        } else {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        if ($code === 500 && $this->container->getParameter('kernel.environment') === "prod") {
            $caller = $this->container->get("error_bundle.logger");
            $caller->setParams(array(
                "product" => "test",
                "component" => "bundles",
                "summary" => $exception->getFile() . " Line " . $exception->getLine(),
                "version" => "1.0",
                "op_sys" => php_uname("s"),
                "platform" => "Other",
                "description" => $exception->getFile() . " Line " . $exception->getLine() . "\n" .
                    $exception->getMessage() . "\n" .
                    $exception->getTraceAsString() . "\n"
            ));
            $caller->call();
        }
    }

}